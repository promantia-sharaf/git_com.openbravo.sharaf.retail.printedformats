/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global */

enyo.kind({
  name: 'OB.UI.ModalEmailInformation',
  kind: 'OB.UI.ModalAction',
  hideCloseButton: true,
  autoDismiss: false,
  closeOnEscKey: false,
  i18nHeader: '',
  handlers: {
    
  },
  bodyContent: {
    kind: 'Scroller',
    maxHeight: '225px',
    thumb: true,
    horizontal: 'hidden',
    components: [{
      name: 'email'
    }, {
        kind: 'emailInformation',
    }]
  },
  bodyButtons: {
    components: [{
      kind: 'OB.UI.ModalEmailInfoOk'
    }]
  },
  executeOnHide: function() {
    //OB.UTIL.HookManager.callbackExecutor(this.args.args, this.args.callbacks);
    //this.args.args.order.save();
    OB.UTIL.HookManager.callbackExecutor(this.args.args, this.args.callbacks);    
  },
  executeOnShow: function () {
    //this.$.bodyContent.$.attributes.$.line_bankBinNumberTextBox.$.newAttribute.$.bankBinNumberTextBox.setValue('');
    this.$.bodyContent.$.email.setContent('Invoice will be sent to below Email ID - ' + this.args.emaildata + '. Please verify Email ID with Customer');
  },
  applyChanges: function (inSender, inEvent) {
    
  },
  initComponents: function () {
    this.inherited(arguments);
    this.attributeContainer = this.$.bodyContent.$.attributes;
  }
});

enyo.kind({
  name: 'emailInformation',
  components: [{

  }],

  init: function () {
    this.inherited(arguments);
  }
});


enyo.kind({
  kind: 'OB.UI.ModalDialogButton',
  name: 'OB.UI.ModalEmailInfoOk',
  tap: function() {
    var me = this;
    me.doHideThisPopup();
    },
  initComponents: function() {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('OBPOS_LblOk'));
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'OB.UI.ModalEmailInformation',
  name: 'OB_UI_ModalEmailInformation'
});