
(function () {
    OB.UTIL.HookManager.registerHook('OBPRINT_PrePrint', function (args, callbacks) {

        if (args.order.get('shfptIsDuplicatePrint') && OB.MobileApp.model.get('terminal').defaultbp_bpcountry_name === 'United Arab Emirates') {
         OB.SHFPT.Utils.checkDGMember(args,callbacks);
        } else {
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
      });
}());