/*global OB, enyo, _ */

(function () {
    OB.UTIL.HookManager.registerHook('OBPOS_PrePrintPaidReceipt', function (args, callbacks) {
       args.receipt.set('shfptIsDuplicatePrint', true);
       OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      });
}());