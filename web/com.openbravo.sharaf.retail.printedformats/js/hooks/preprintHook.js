/*global OB, enyo, _ */

(function () {
    OB.UTIL.HookManager.registerHook('OBPRINT_PrePrint', function (args, callbacks) {

        if(OB.MobileApp.model.get('terminal').shfptDisbalePrint && !args.order.get('shfptIsDuplicatePrint')){
          args.cancelOperation = true;
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);             
        } else {
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
      });
}());