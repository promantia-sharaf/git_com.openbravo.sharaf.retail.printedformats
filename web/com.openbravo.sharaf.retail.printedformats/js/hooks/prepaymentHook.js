/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

(function() {
  OB.UTIL.HookManager.registerHook('OBPOS_PrePaymentHook', function(
    args,
    callbacks
  ) {
    var receipt = args.context.get('order'),
        lines = receipt.get('lines');
    var digitalProductPresent = false;
    
      _.each(lines.models, function (line) {
           if(!OB.UTIL.isNullOrUndefined(line.get('product').get('digiProductType')) && (line.get('product').get('digiProductType') === 'P' || 
               line.get('product').get('digiProductType') === 'A' || line.get('product').get('digiProductType') === 'E' || line.get('product').get('digiProductType') === 'SP' 
               || line.get('product').get('digiProductType') === 'SA') && (!receipt.get('isVerifiedReturn') || !receipt.get('isQuotation'))) {
             digitalProductPresent = true;
          }
       });
    var callback = function() {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    };
    if (
      receipt.get('custsdtDocumenttype') &&
      OB.COSRD.getSearchKeyFromDocumentType(
        receipt.get('custsdtDocumenttype')
      ) &&
      OB.COSRD.getSearchKeyFromDocumentType(receipt.get('custsdtDocumenttype'))
        .ticketSequence &&
      !receipt.get('isVerifiedReturn') && receipt.get('isEditable') && !digitalProductPresent
    ) {
      if (args.context.get('order').get('custshaCustomerEmail')) {
        OB.MobileApp.view.waterfallDown('onShowPopup', {
            popup: 'OB_UI_ModalEmailInformation',
            args:{
               emaildata : args.context.get('order').get('custshaCustomerEmail'),
               args: args,
               callbacks: callbacks,
             }
            });
      } else {
         OB.MobileApp.view.waterfallDown('onShowPopup', {
             popup: 'OB_UI_ModalEmailInformation',
             args:{
             emaildata : args.context.get('order').get('bp').get('email'),
                 args: args,
                 callbacks: callbacks,
             }
         });
      }
    } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });
})();
