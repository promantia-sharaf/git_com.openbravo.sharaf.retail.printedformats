/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  OB.SHFPT = OB.SHFPT || {};

  OB.SHFPT.Utils = {

    getGroupedRedemptionPayments: function (order) {
      var redemptions = [],
          payments = order.get('payments').models;
      _.each(payments, function (p) {
        p.set('custdisUsed', false);
      });
      _.each(order.get('lines').models, function (line) {
        if (line.get('custdisIrType') === 'IRA' || line.get('custdisIrType') === 'IRB') {
          var payment = _.find(payments, function (p) {
            return !p.get('custdisUsed') && p.get('paymentData') && p.get('paymentData').lineId === line.get('custdisOrderline');
          });
          if (payment) {
            payment.set('custdisUsed', true);
            var paymentRedempt = _.find(redemptions, function (pr) {
              return pr.id === line.get('product').id;
            });
            if (!paymentRedempt) {
              redemptions.push({
                id: line.get('product').id,
                name: payment.get('name'),
                isocode: payment.get('isocode'),
                amount: payment.get('amount')
              });
            } else {
              paymentRedempt.amount += payment.get('amount');
            }
          }
        }
      });
      return redemptions;
    },

    getPromotionLineInfo: function (line) { 
      var promotion = {
        manualDiscount: false,
        sharafDiscount: false,
        hasPromo: false,
        sharafPromoAmount: 0.0,
        manualDiscountAmt: 0.0
      };
      _.each(line.get('promotions'), function (p) {
        promotion.hasPromo = true;
        if (p.discountType === 'CA5491E6000647BD889B8D7CDF680795' || p.discountType === '6A3C2313136147A6B2D1B8D2F5F768E6') {
          promotion.sharafDiscount = true;
          promotion.sharafPromoAmount += p.amt;
        } else {
          promotion.manualDiscount = true;
          promotion.manualDiscountAmt += p.amt;
        }
      });
      if (line.get('custdisOffer')) {
        promotion.hasPromo = true;
        promotion.sharafDiscount = true;
      }
      return promotion;
    },



    getDescriptionsLines: function (description) {
      var result = [],
          line = description;
      while (line.length > 0) {
        if (result.length === 11) {
          line = '';
        } else {
          if (line.length > 25) {
            var temp = line.substring(0, 25),
                indx = temp.lastIndexOf(' ');
            if (indx > 0) {
              result.push(temp.substring(0, indx));
              line = line.substring(indx + 1);
            } else {
              result.push(temp);
              line = line.substring(25);
            }
          } else {
            result.push(line);
            line = '';
          }
        }
      }
      return result;
    },

   checkDGMember : function(args,callbacks) {
		 var incr =0;
		 var set  =  new Set();
    	 var bcatId = '609FCB2411494860ADE86DB777488B5F'; // DG Member
       
      var lin =  _.filter(args.order.get('lines').models, function(line) {
                         return !OB.UTIL.isNullOrUndefined(line.get('custdisOffer'))
         });
      if(lin.length > 0)  {
         _.each(lin, function(line) {
           var mOffer = line.get('custdisOffer')
         new OB.DS.Process('com.openbravo.sharaf.retail.printedformats.process.ShfptReceiptProperties').exec({
       		 category : bcatId,
       		 mOffer : mOffer,
       		 clientId: OB.MobileApp.model.get('terminal').client,
             orgid: OB.MobileApp.model.get('terminal').organization
  		  }, function(data) {
             if(data && data.DGPromo) {
               incr ++;
               line.set('DGPromotion',true);
               if(incr === lin.length){
      	      	 OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        	   }
             } else {
               incr ++;
               if(incr === lin.length){
        	      	 OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        	    }
             }
         });
          
        });

      }  else {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
   },
   getLoyaltyDiscountOfferName: function (order) {
       var promoname = [];
       var lin =  _.filter(order.get('lines').models, function(line) {
          return !OB.UTIL.isNullOrUndefined(line.get('custdisLoyaltyDiscountOffer') )
       });
       if(lin.length > 0) {
         k = lin[0].get('promotions');
         k.forEach(function (p) {  
         if(p.discountType ===  '25ECBE384EAB4C1EB69948FF99FC8456') {
                    promoname.push(p.name); 
                    promoname.push(lin[0].get('loyaltyCardNumber'));
              }
        });
           return promoname;
    }
  },
      
   getLoyaltyDiscount: function (order) {
      var amount = 0;
      enabledLoyaltyPayMethod = new Array();
        if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('enabledLoyaltyDiscountPayMethod')) && !OB.UTIL.isNullOrUndefined(localStorage.getItem('enabledLoyaltyDiscountPayMethod')).enabledLoyaltyDiscountPayMethod) {
           enabledLoyaltyPayMethod = JSON.parse(localStorage.getItem('enabledLoyaltyDiscountPayMethod')).enabledLoyaltyDiscountPayMethod;
        }
      order.get('payments').forEach(function (payment) {
       if (!OB.UTIL.isNullOrUndefined(payment.get('kind')) && !OB.UTIL.isNullOrUndefined(payment.get('paymentData'))
          && !OB.UTIL.isNullOrUndefined(enabledLoyaltyPayMethod[payment.get('kind')])) {
            amount = payment.get('amount');          }
      });
      return amount;
    }
   
  };
  
}());
