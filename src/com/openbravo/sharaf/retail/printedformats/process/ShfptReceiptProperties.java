/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.printedformats.process;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.model.common.businesspartner.Category;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.geography.Country;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.pricing.priceadjustment.BusinessPartnerGroup;
import org.openbravo.model.pricing.priceadjustment.PriceAdjustment;
import org.openbravo.retail.posterminal.TerminalType;
import org.openbravo.retail.posterminal.TerminalTypePaymentMethod;

public class ShfptReceiptProperties extends JSONProcessSimple {

  private static Logger log = Logger.getLogger(ShfptReceiptProperties.class);

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {

    JSONObject result = new JSONObject();
    JSONObject data = new JSONObject();
    ArrayList<String> records = new ArrayList<>();
    try {
    	OBContext.setAdminMode(true);
    	String bcatId = jsonsent.getString("category");
    	String	clientId = jsonsent.getString("clientId");
    //	String	orgid = jsonsent.getString("orgid");
    	   
    	String offerid = jsonsent.getString("mOffer");
        	final String sqlString = "select distinct OBPG.m_offer_id from m_offer_bp_group OBPG " //
        	             + " where OBPG.c_bp_group_id = :bcatId" 
        	             + " and OBPG.m_offer_id = :offerid "
        	           //  + " and OBPG.ad_org_id = :orgid "
        	             + " and OBPG.ad_client_id = :clientId " ;
           Session session = OBDal.getInstance().getSession();
           Query query = session.createSQLQuery(sqlString);
           query.setParameter("bcatId", bcatId);
           query.setParameter("offerid",offerid.toString() );
           query.setParameter("clientId",clientId );
          // query.setParameter("orgid",orgid);
           String res = query.uniqueResult() != null ? (String) query.uniqueResult() : null;
           if(res !=null && res != "") {
        	   data.put("DGPromo", "true");
        	   result.put("data", data);
           }
      result.put("status", 0);
    } catch (Exception e) {
      log.error("Error BusinessPartnerGroup Information "
          + e.getMessage(), e);
      result.put("status", 1);
      result.put("message", e.getMessage());
    } finally {
      OBContext.restorePreviousMode();
    }
    return result;
  }

}

	    		